package nl.oefeningen.deel4;

public class PetSearchEngine {

    private final PetRepository petRepository;

    public PetSearchEngine(final PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    public String petNamesOfSpeciesToString(final String species) {
        throw new UnsupportedOperationException("Implement me!");
    }

}
