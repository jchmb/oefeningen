package nl.oefeningen.deel4;

public class Pet {
    private final String name;
    private final String species;

    public Pet(String name, String species) {
        this.name = name;
        this.species = species;
    }

    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }
}
