package nl.oefeningen.deel4;

import java.util.List;

/**
 * We don't even need an implementation of this interface for the unit test of the PetSearchEngine.
 */
public interface PetRepository {
    List<Pet> findAllBySpecies(String species);
}
