package nl.oefeningen.deel3;

public enum Outcome {
    PLAYER1_WIN,
    PLAYER2_WIN,
    DRAW
}
