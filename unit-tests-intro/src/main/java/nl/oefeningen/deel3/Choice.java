package nl.oefeningen.deel3;

public enum Choice {
    ROCK,
    PAPER,
    SCISSORS
}
