package nl.oefeningen.deel3;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

/**
 * Rock-paper-scissors.
 *
 * Rock beats scissors.
 * Scissors beats paper.
 * Paper beats rock.
 * Same moves are a draw.
 */
class RockPaperScissorsTest {

    /**
     * Given rock and scissors, when play, then expect player 1 wins.
     */
    @Test
    void given_rockVersusScissors_when_play_then_expectPlayer1Wins() {
        assertThat(RockPaperScissors.play(Choice.ROCK, Choice.SCISSORS), is(Outcome.PLAYER1_WIN));
    }

    /**
     * Given paper and rock, when play, then expect player 1 wins.
     */
    @Test
    void given_paperVersusRock_when_play_then_expectPlayer1Wins() {
        assertThat(RockPaperScissors.play(Choice.PAPER, Choice.ROCK), is(Outcome.PLAYER1_WIN));
    }

    /**
     * Given scissors and paper, when play, then expect player 1 wins.
     */
    @Test
    void given_scissorsVersusPaper_when_play_then_expectPlayer1Wins() {
        assertThat(RockPaperScissors.play(Choice.SCISSORS, Choice.PAPER), is(Outcome.PLAYER1_WIN));
    }

    /**
     * Given paper and scissors, when play, then expect player 2 wins.
     */
    @Test
    void given_paperVersusScissors_when_play_then_expectPlayer2Wins() {
        assertThat(RockPaperScissors.play(Choice.PAPER, Choice.SCISSORS), is(Outcome.PLAYER2_WIN));
    }

    /**
     * Given rock and rock, when play, then expect draw.
     */
    @Test
    void given_rockVersusRock_when_play_then_expectDraw() {
        assertThat(RockPaperScissors.play(Choice.ROCK, Choice.ROCK), is(Outcome.DRAW));
    }

    /**
     * Given paper and paper, when play, then expect draw.
     */
    @Test
    void given_paperVersusPaper_when_play_then_expectDraw() {
        assertThat(RockPaperScissors.play(Choice.PAPER, Choice.PAPER), is(Outcome.DRAW));
    }

    /**
     * Given scissors and scissors, when play, then expect draw.
     */
    @Test
    void given_scissorsVersusScissors_when_play_then_expectDraw() {
        assertThat(RockPaperScissors.play(Choice.SCISSORS, Choice.SCISSORS), is(Outcome.DRAW));
    }

}