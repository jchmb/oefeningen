package nl.oefeningen.deel5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class Factorial5Test {

    /**
     * If n < 0, then throw an IllegalArgumentException, since negative factorials are undefined for the real numbers.
     */
    @Test
    void given_aNegativeNumber_when_factorial_then_expectIllegalArgumentExceptionToBeThrown() {
        assertThrows(IllegalArgumentException.class, () -> Factorial.factorial(-1));
    }
}