package nl.oefeningen.deel5;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class Factorial3Test {

    /**
     * 3! = 3 * 2 * 1 = 6
     */
    @Test
    void given_two_when_factorial_then_expectTwo() {
        assertThat(Factorial.factorial(2), is(2));
    }
}