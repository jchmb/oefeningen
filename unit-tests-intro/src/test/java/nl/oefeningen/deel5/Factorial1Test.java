package nl.oefeningen.deel5;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class Factorial1Test {

    /**
     * 0! = 1
     */
    @Test
    void given_zero_when_factorial_then_expectOne() {
        assertThat(Factorial.factorial(0), is(1));
    }

    /**
     * 1! = 1
     */
    @Test
    void given_one_when_factorial_then_expectOne() {
        assertThat(Factorial.factorial(1), is(1));
    }
}