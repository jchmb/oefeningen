package nl.oefeningen.deel5;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class Factorial2Test {

    /**
     * 2! = 2 * 1 = 2
     */
    @Test
    void given_two_when_factorial_then_expectTwo() {
        assertThat(Factorial.factorial(2), is(2));
    }
}