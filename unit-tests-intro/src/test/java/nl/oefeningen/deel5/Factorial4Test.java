package nl.oefeningen.deel5;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class Factorial4Test {

    @ParameterizedTest
    @MethodSource("testData")
    void given_anInput_when_factorial_then_expectOutput(final int input, final int output) {
        assertThat(Factorial.factorial(input), is(output));
    }

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(0, 1),
                Arguments.of(1, 1),
                Arguments.of(2, 2),
                Arguments.of(3, 6),
                Arguments.of(4, 24),
                Arguments.of(5, 120)
        );
    }
}