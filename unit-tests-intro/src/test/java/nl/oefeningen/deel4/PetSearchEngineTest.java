package nl.oefeningen.deel4;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This unit test uses Mockito to mock the PetRepository. We don't even need an implementation of it.
 * We just need to tell Mockito what to return when the findAllBySpecies method is called.
 * That way we can still test the PetSearchEngine without having to implement the PetRepository.
 */
class PetSearchEngineTest {

    private final PetRepository petRepository = mock(PetRepository.class);
    private final PetSearchEngine sut = new PetSearchEngine(petRepository);

    /**
     * There are no pets of the given species, therefore the result should be an empty string.
     */
    @Test
    void given_noPets_when_petNamesOfSpeciesToString_then_expectAnEmptyString() {
        assertThat(sut.petNamesOfSpeciesToString("cat"), is(""));
    }

    /**
     * There is one pet of the given species, therefore the result should be the name of that pet.
     */
    @Test
    void given_aSingleKnownCat_when_petNamesOfSpeciesToString_then_expectFluffy() {
        when(petRepository.findAllBySpecies("cat"))
                .thenReturn(List.of(new Pet("Fluffy", "cat")));

        assertThat(sut.petNamesOfSpeciesToString("cat"), is("Fluffy"));
    }

    /**
     * There are two pets of the given species, therefore the result should be the names of those pets, separated by a comma.
     */
    @Test
    void given_twoDogs_when_petNamesOfSpeciesToString_then_expectFidoAndRex() {
        when(petRepository.findAllBySpecies("dog"))
                .thenReturn(List.of(
                        new Pet("Fido", "dog"),
                        new Pet("Rex", "dog")
                ));

        assertThat(sut.petNamesOfSpeciesToString("dog"), is("Fido, Rex"));
    }

    /**
     * There is a cat and there are two dogs, therefore the result should be the names of those pets, separated by a comma, grouped by species.
     */
    @Test
    void given_aCatAndTwoDogs_when_petNamesOfSpeciesToString_then_expectFluffyAndFidoAndRex() {
        when(petRepository.findAllBySpecies("dog"))
                .thenReturn(List.of(
                        new Pet("Fido", "dog"),
                        new Pet("Rex", "dog")
                ));
        when(petRepository.findAllBySpecies("cat"))
                .thenReturn(List.of(new Pet("Fluffy", "cat")));

        assertThat(sut.petNamesOfSpeciesToString("dog"), is("Fido, Rex"));
        assertThat(sut.petNamesOfSpeciesToString("cat"), is("Fluffy"));
    }
}