package nl.oefeningen.deel2;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class PalindromeTest {

    /**
     * Given an empty string, when isPalindrome, then expect true.
     */
    @Test
    void given_anEmptyString_when_isPalindrome_then_expectTrue() {
        assertThat(Palindrome.isPalindrome(""), is(true));
    }

    /**
     * Given a single character string, when isPalindrome, then expect true.
     */
    @Test
    void given_aSingleCharacterString_when_isPalindrome_then_expectTrue() {
        assertThat(Palindrome.isPalindrome("a"), is(true));
    }

    /**
     * Given two different characters, when isPalindrome, then expect true.
     */
    @Test
    void given_twoDifferentCharacters_when_isPalindrome_then_expectFalse() {
        assertThat(Palindrome.isPalindrome("ab"), is(false));
    }

    /**
     * Given abba, when isPalindrome, then expect true.
     */
    @Test
    void given_abba_when_isPalindrome_then_expectTrue() {
        assertThat(Palindrome.isPalindrome("abba"), is(true));
    }

    /**
     * Given abcba, when isPalindrome, then expect true.
     */
    @Test
    void given_abcba_when_isPalindrome_then_expectTrue() {
        assertThat(Palindrome.isPalindrome("abcba"), is(true));
    }

}