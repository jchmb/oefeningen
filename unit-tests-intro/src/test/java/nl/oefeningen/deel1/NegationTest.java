package nl.oefeningen.deel1;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class NegationTest {

    /**
     * Given true, when negate, then expect false.
     */
    @Test
    void given_true_when_negate_then_expectFalse() {
        assertThat(Negation.negate(true), is(false));
    }

    /**
     * Given false, when negate, then expect true.
     */
    @Test
    void given_false_when_negate_then_expectTrue() {
        assertThat(Negation.negate(false), is(true));
    }

}