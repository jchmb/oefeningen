Oefeningen
==========

Dit project bevat een aantal oefeningen die je kan gebruiken om je kennis van unit-testing te testen en bij te spijkeren. De oefeningen zijn opgedeeld in verschillende stappen in de vorm van packages.
Momenteel zijn er 4 delen. Probeer ze 1 voor 1 op te lossen. Het doel is simpel: laat de tests voor ieder deel slagen.

Het is aangeraden om een IDE te gebruiken. IntelliJ heeft de voorkeur: https://www.jetbrains.com/idea/download

Java 17+ (JDK) is een vereiste. Je kan de laatste versie downloaden op bijvoorbeeld https://adoptopenjdk.net/ of https://www.oracle.com/java/technologies/downloads/#jdk19-windows

Deel 1
------
Een simpele functie om de negatie van een boolean te bepalen. Dit is puur om je bekend te maken met het unit-testen.

Deel 2
------
Dit is een bekend programmeerprobleem wat je nu moet oplossen door de unit-tests te laten slagen.

Deel 3
------
Dit is een implementatie van rock-paper-scissors. De unit-tests zijn al geschreven, maar de implementatie is nog niet af.

Deel 4
------
Hier moet je werken met een repository, waarvan alleen de interface gegeven is. Het punt is om bekend te raken met mocking.

Deel 5
------
Het uitrekenen van de faculteit van een getal. Dit is een bekend probleem dat je nu moet oplossen door de unit-tests te laten slagen.
Bijvoorbeeld 0! = 1, 1! = 1, 2! = 2 * 1 = 2, 3! = 3 * 2 * 1 = 6, 4! = 4 * 3 * 2 * 1 = 24, etc.
Dit is meer gefocust op test-driven development, waarbij je steeds afwisselt tussen het schrijven van unit-tests en het schrijven van productiecode.
Het idee is: schrijf niet meer code dan nodig is om de test te laten slagen, ook als het tegen je intuitie ingaat.
Iedere keer als je alle tests van een test-klasse laat slagen, kan je de volgende test-klasse aanpakken. Dus begin bij Factorial1Test en werk naar Factorial5Test toe.